#include <stdio.h>

#define cr_entrypoint
#include <corrosion/cr.h>

const char* pak_name = "game.pak";

struct {
	struct res_pak* pak;

	struct pipeline* pipeline;

	struct vertex_buffer* vb;
} app;

struct vertex {
	v2f position;
	v2f uv;
};

struct app_config cr_config() {
	return (struct app_config) {
		.name = "Game",
		.video_config = (struct video_config) {
			.api = video_best_api(video_feature_base),
#ifdef debug
			.enable_validation = true,
#else
			.enable_validation = false,
#endif
			.clear_colour = make_rgba(0x000000, 255)
		},
		.window_config = (struct window_config) {
			.title = "Game",
			.size = make_v2i(1366, 768),
			.resizable = true
		}
	};
}

void cr_init() {
	app.pak = pak_open(pak_name, 0);
	res_use_pak(app.pak);

	struct shader* shader = load_shader("shaders/triangle.csh", null);
	struct texture* vulkan_logo = load_texture("sprites/vulkan-logo-1.png", texture_flags_filter_linear, null);

	app.pipeline = video.new_pipeline(
		pipeline_flags_draw_tris,
		shader,
		video.get_default_fb(),
		(struct pipeline_attribute_bindings) {
			.bindings = (struct pipeline_attribute_binding[]) {
				{
					.attributes = (struct pipeline_attributes) {
						.attributes = (struct pipeline_attribute[]) {
							{
								.name     = "position",
								.location = 0,
								.offset   = offsetof(struct vertex, position),
								.type     = pipeline_attribute_vec2
							},
							{
								.name     = "uv",
								.location = 1,
								.offset   = offsetof(struct vertex, uv),
								.type     = pipeline_attribute_vec2
							},
						},
						.count = 2
					},
					.stride = sizeof(struct vertex),
					.rate = pipeline_attribute_rate_per_vertex,
					.binding = 0
				}
			},
			.count = 1
		},
		(struct pipeline_descriptor_sets) {
			.sets = (struct pipeline_descriptor_set[]) {
				{
					.name = "primary",
					.descriptors = (struct pipeline_descriptor[]) {
						{
							.name     = "image",
							.binding  = 0,
							.stage    = pipeline_stage_fragment,
							.resource = {
								.type    = pipeline_resource_texture,
								.texture = vulkan_logo
							}
						},
					},
					.count = 1,
				}
			},
			.count = 1
		}
	);

	struct vertex verts[] = {
		{ {  0.0f,  0.5f }, { 0.5f, 0.0f } },
		{ { -0.5f, -0.5f }, { 0.0f, 1.0f } },
		{ {  0.5f, -0.5f }, { 1.0f, 1.0f } },
	};

	app.vb = video.new_vertex_buffer(verts, sizeof verts, vertex_buffer_flags_none);
}

void cr_update(f64 ts) {
	static f64 timer = 0.0;
	timer += ts;
	if (timer >= 1.0) {
		timer = 0.0;
		printf("%g\n", 1.0 / ts);
	}

	video.begin_framebuffer(video.get_default_fb());
		video.begin_pipeline(app.pipeline);
			video.bind_pipeline_descriptor_set(app.pipeline, "primary", 0);
			video.bind_vertex_buffer(app.vb, 0);
			video.draw(3, 0, 1);
		video.end_pipeline(app.pipeline);
	video.end_framebuffer(video.get_default_fb());
}

void cr_deinit() {
	video.free_pipeline(app.pipeline);
	video.free_vertex_buffer(app.vb);

	pak_close(app.pak);
}
