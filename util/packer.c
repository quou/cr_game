#include <stdio.h>

#include <corrosion/cr.h>

vector(struct pak_write_file) writes;
char res_dir[1024];

void add_dir(const char* dir) {
	struct dir_iter* i = new_dir_iter(dir);
	if (i) {
		do {
			struct dir_entry* entry = dir_iter_cur(i);

			if (entry->info.type == file_directory) {
				add_dir(entry->name);
			} else if (entry->info.type == file_normal) {
				char* dst_name = entry->name;
				usize name_len = strlen(entry->name);

				for (char* c = dst_name; *c; c++) {
					if (*c == '\\') {
						*c = '/';
					}
				}

				for (usize j = 0; j < name_len; j++) {
					if (*dst_name == res_dir[j]) {
						dst_name++;
					} else {
						break;
					}
				}

				while (*dst_name == '/') {
					dst_name++;
				}

				char* src = copy_string(entry->name);
				char* dst = copy_string(dst_name);

				vector_push(writes,
					((struct pak_write_file) {
						.src = src,
						.dst = dst
					})
				);
			}
		} while (dir_iter_next(i));

		free_dir_iter(i);
	}
}

i32 main(i32 argc, const char** argv) {
	alloc_init();

	if (argc < 3) {
		printf("Usage: %s resource_dir executable", argv[0]);
	}

	writes = null;

	strcpy(res_dir, argv[1]);

	for (char* c = res_dir; *c; c++) {
		if (*c == '\\') {
			*c = '/';
		}
	}

	add_dir(res_dir);

	char inter[1024];
	strcpy(inter, argv[2]);
	strcat(inter, ".pak");

	if (!write_pak(inter, writes, vector_count(writes))) {
		abort_with("Failed to write PAK.");
	}

	for (usize i = 0; i < vector_count(writes); i++) {
		core_free((void*)writes[i].src);
		core_free((void*)writes[i].dst);
	}

	free_vector(writes);

	alloc_deinit();
}
